# Rechner
Aufgabe Rechner für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Implementieren Sie eine einfache additionsrechnung in der Methode "rechner" innerhalb der Klasse "Program" Rechnen\Program.cs

## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
