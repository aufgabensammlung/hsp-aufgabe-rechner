using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rechnen;
using System;

namespace UnitTest_Rechnen
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            Program prg = new Program();

            // Act 
            var result = prg.rechner(10,5);

            // Assert
            Assert.AreEqual(result, 15);
        }
    }
    
}
